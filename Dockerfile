FROM maven:3.5.2-jdk-8

COPY target/HelloWorld-1.jar ./HelloWorld.jar

EXPOSE 8000

CMD ["java", "-jar", "HelloWorld.jar"]
